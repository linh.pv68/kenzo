<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
//$route['default_controller'] = 'welcome';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;
/*Define by Linhpv*/
$route['default_controller'] = 'home/index';
$route['register'] = 'home/register';
$route['login'] = 'home/login';
$route['logout'] = 'home/logout';
$route['home'] = 'home/index';

$route['pusher'] = 'pusher/push';
$route['pusher/anser'] = 'pusher/pushAdmin';

$route['admin/login'] = 'admin/login';
$route['admin/logout'] = 'admin/logout';
$route['admin/dashboard'] = 'admin/dashboard';
$route['admin/charts'] = 'Scores/getCharts';
$route['admin/list-user'] = 'admin/getListUser';
$route['admin/user/delete'] = 'admin/deleteCustomer';

$route['question'] = 'question/index';
$route['question/add'] = 'question/add';
$route['question/delete'] = 'question/delete';
$route['question/edit/:id'] = 'question/edit';
$route['question/edit/sua'] = 'question/sua';

$route['get_list_option'] = 'CodeQuestion/getListOption';
$route['code-question'] = 'CodeQuestion/View';
$route['code-question/get-list'] = 'CodeQuestion/getList';
$route['code-question/add'] = 'CodeQuestion/add';
$route['code-question/edit'] = 'CodeQuestion/edit';
$route['code-question/delete'] = 'CodeQuestion/delete';
$route['code-question/get-by-id'] = 'CodeQuestion/getById';
$route['code-question/check-pin-code'] = 'CodeQuestion/checkPinCodeQuestion';

$route['game/index']['POST'] = 'game/index';
$route['game/play/:id'] = 'game/play';
$route['answer-customer'] = 'AnswerCustomer/addAnswer';
$route['admin/reset-score'] = 'Scores/resetScore';
$route['get-charts-scores'] = 'Scores/getChartsScores';


/*Cuong*/
$route['del'] = 'admin/del';

