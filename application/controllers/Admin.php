<?php


class Admin extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->Model("Madmin");
		$this->load->Model("Customer");
		$this->load->Model("AdminModel");
		$this->load->Model("CodeQuestionModel");
		$this->load->library('session');

	}

	public function index()
	{
		redirect(base_url() . 'admin/login');

	}

	public function login()
	{
		$userInfo = false;
		$data['message'] = "Tài khoản hoặc mật khẩu không chính xác";
		$this->getRequest();

		if ($this->session->userdata("userAdminInfo")) {
			return redirect(base_url() . 'admin/dashboard');
		}
		if ($this->getRequest()) {
			$params = $this->getRequest();
			if (isset($params['username']) && isset($params['password'])) {
				if ($params['username'] && $params['password']) {
					$userInfo = $this->AdminModel->login($params);
				}
			}
			if ($userInfo) {
				$userData = $userInfo;
				$userData['logged_in'] = true;
				$this->session->set_userdata('userAdminInfo', $userData);
				redirect(base_url() . 'admin/dashboard');
			} else {
				$this->load->view(self::AREA_BACKEND . '/login', $data);
			}
		} else {
			$this->load->view(self::AREA_BACKEND . '/login');
		}

	}

	public function logout()
	{
		$userInfo = $this->session->userdata("userAdminInfo");
		if ($userInfo) {
			$this->session->unset_userdata('userAdminInfo');
			return redirect('admin/login', "refresh");
		}
	}

	public function dashboard()
	{
		$pinCodes = $this->CodeQuestionModel->getList();
		$data = array();
		if ($pinCodes) {
			foreach ($pinCodes as $value) {
				if (isset($value['code'])) {
					$code = $value['code'];
				}
				if (isset($value['title'])) {
					$title = $value['title'];
				}
				if (isset($value['id'])) {
					$id = $value['id'];
				}
				if (isset($code) && isset($title) && isset($id)) {
					$data['pin_codes'][] = array(
						"id" => $id,
						"code" => $code,
						"title" => $title
					);
				}


			}
		}
		$this->load->view(self::AREA_BACKEND . '/dashboard', $data);
	}

	public function getListUser()
	{
		$listUser = $this->Customer->getList();
		$data['list_user'] = $listUser;
		return $this->load->view('backend/user/view', $data);
	}

	public function deleteCustomer()
	{
		$resultData = [
			'success' => false,
			'data' => array(),
			'error' => true
		];
		$userId = $this->input->post('userId');
		if ($userId) {
			if ($this->Customer->delete($userId)) {
				$resultData['success'] = true;
				$resultData['error'] = false;

			} else {
				$resultData['data']['message'] = "Đã xảy ra lỗi trong quá trình xóa";
			}
		}

		echo json_encode($resultData);

	}


}
