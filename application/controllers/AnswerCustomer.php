<?php


class AnswerCustomer extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->model('AnswerCustomerModel');
	}

	public function addAnswer()
	{
		$resultJson = array(
			'success' => false,
			'data' => [],
			'error' => ''
		);

		if ($this->getRequest()) {
			$params = $this->getRequest();
			$result = $this->AnswerCustomerModel->add($params);
			if ($result['success']) {
				$resultJson['success'] = true;
				$resultJson['data']['message'] = "Câu trả lời của bạn đã được ghi nhận.";
				$resultJson['error'] = false;
			} else {
				$resultJson['data']['message'] = "Lỗi trong quá trình lưu câu hỏi.";
			}
		}
		echo json_encode($resultJson);
	}

}
