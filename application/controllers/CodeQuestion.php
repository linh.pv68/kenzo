<?php


class CodeQuestion extends CI_Controller
{

	protected $resultJson;

	public function __construct()
	{
		parent::__construct();
		$this->load->Model("CodeQuestionModel");
		$this->resultJson = array(
			'success' => true,
			'data' => array(),
			'error' => false
		);

	}

	public function view()
	{
		$this->load->view(self::AREA_BACKEND . '/codeQuestion/view');
	}

	public function getList()
	{
		$data = $this->CodeQuestionModel->getList();
		try {
			if ($data) {
				$codeHtml = '';
				foreach ($data as $key => $value) {
					$number = $key + 1;
					$codeHtml .= '
					<tr>
						<td>' . $number . '</td>
						<td>' . $value['code'] . '</td>
						<td>' . $value['title'] . '</td>
						<td>' . $value["des"] . '</td>
						<td>
							<button data-id="' . $value["id"] . '" class="btn btn-warning showDataEdit" style="padding: 6px 4px; margin-right:4px;">Sửa</button>
							<button data-id="' . $value["id"] . '" class="btn btn-danger deleteData" style="padding: 6px 4px; margin-right:4px;">Xóa</button>
						</td>
					</tr>
				';
				}
				$this->resultJson['success'] = true;
				$this->resultJson['data'] = $codeHtml;
				$this->resultJson['error'] = false;
			}
		} catch (Exception $exception) {
			$this->resultJson['success'] = false;
			$this->resultJson['data']['message'] = $exception->getMessage();
			$this->resultJson['error'] = true;
		}
		echo json_encode($this->resultJson);
	}

	public function add()
	{
		$code = $this->input->post("code");
		$name = $this->input->post("name");
		$des = $this->input->post("des");
		$data = array(
			"code" => $code,
			"title" => $name,
			"des" => $des
		);
		$this->action('add', $data);

	}

	public function edit()
	{
		$code = $this->input->post("code");
		$name = $this->input->post("name");
		$des = $this->input->post("des");
		$id = $this->input->post("id");
		$data = array(
			"code" => $code,
			"title" => $name,
			"des" => $des,
			"id" => $id
		);
		$this->action('edit', $data);

	}

	public function delete()
	{
		$id = $this->input->post("id");
		try {
			$statusDelete = $this->CodeQuestionModel->delete($id);
			if ($statusDelete) {
				$this->resultJson['success'] = true;
				$this->resultJson['data']['message'] = "Delete the code question success";
				$this->resultJson['error'] = false;
			} else {
				$this->resultJson['success'] = false;
				$this->resultJson['data']['message'] = "An error in process delete code question.";
				$this->resultJson['error'] = true;
			}

		} catch (Exception $exception) {
			$this->resultJson['success'] = false;
			$this->resultJson['data']['message'] = $exception->getMessage();
			$this->resultJson['error'] = true;
		}
		echo json_encode($this->resultJson);

	}

	public function getById()
	{
		$id = $this->input->post("id");
		$data = $this->CodeQuestionModel->getById($id);
		echo json_encode($data);
	}


	public function getListOption()
	{
		$result = array(
			'success' => true,
			'data' => array(),
			'error' => false
		);
		$data = $this->CodeQuestionModel->getList();
		try {
			$optionHtml = "<option selected>---</option>";
			foreach ($data as $key => $value) {
				$optionHtml .= '<option value="' . $value['id'] . '">' . $value['title'] . '</option>';
			}
			$result['data'] = $optionHtml;
		} catch (Exception $exception) {
			$result['error'] = true;
			$result['success'] = false;
			$result['data']['message'] = $exception->getMessage();
		}

		echo json_encode($result);
	}

	private function action($method, $data)
	{
		try {
			switch ($method) {
				case 'add':
					$statusQuery = $this->CodeQuestionModel->add($data);
					break;
				case 'edit':
					$statusQuery = $this->CodeQuestionModel->edit($data);
					break;
			}

			if ($statusQuery) {
				$this->getList();
			} else {
				$this->resultJson['success'] = false;
				$this->resultJson['data']['message'] = "An error in process.";
				$this->resultJson['error'] = true;
				echo json_encode($this->resultJsons);
			}
		} catch (Exception $exception) {
			$this->resultJson['success'] = false;
			$this->resultJson['data']['message'] = $exception->getMessage();
			$this->resultJson['error'] = true;
			echo json_encode($this->resultJson);
		}
	}

	public function checkPinCodeQuestion(){
		if ($this->getRequest()){
			$params = $this->getRequest();
			if (isset($params['pin_code'])){
				$pinCode = $params['pin_code'];
				$checkPinCode = $this->CodeQuestionModel->checkPinCode($pinCode);
				if ($checkPinCode){
					$this->resultJson['success'] = true;
					$this->resultJson['data'] = $checkPinCode;
					$this->resultJson['error'] = false;
				}else{
					$this->resultJson['success'] = false;
					$this->resultJson['data']['message'] = "Mã Pin Không Hợp Lệ.";
					$this->resultJson['error'] = true;
				}
				echo json_encode($this->resultJson);
			}
		}
	}

}
