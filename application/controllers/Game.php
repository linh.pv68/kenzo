<?php


class Game extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->model('QuestionModel');
	}

	public function index()
	{
		if ($this->getRequest()) {
			$params = $this->getRequest();
			if (isset($params['code_id'])) {
				$codeId = $params['code_id'];
				//get cau hoi thuoc bo cau hoi da chon
				$listQuestion = $this->QuestionModel->getByCodeId($codeId);
				if ($listQuestion) {
					$data['list_question'] = $listQuestion;
					$this->load->view('backend/game/index', $data);
				} else {
					$this->load->view('backend/game/index');
				}
			}
		} else {
			echo 'Lỗi, Vui lòng kiểm tra lại';
			exit;
		}
	}

	public function play($id)
	{
		if ($id) {
			$question = $this->QuestionModel->getById($id);
			$data['question'] = $question;
			$this->load->view('backend/game/play', $data);
		}
	}

}
