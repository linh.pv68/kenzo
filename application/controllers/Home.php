<?php

class Home extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->model('customer');
		$this->load->library('session');
	}

	public function index()
	{
		$userInfo = $this->session->userdata("userInfo");
		$viewHtml = "login";
		if ($userInfo) {
			$viewHtml = "home";
		}
		$this->load->view(self::AREA_FRONTEND . '/' . $viewHtml);
	}

	public function register()
	{
		$paramsRegister = $this->getRequest();
		$message = "";
		if ($paramsRegister) {
			if (isset($paramsRegister['password']) && isset($paramsRegister['re_pass'])) {
				if ($paramsRegister['password'] != $paramsRegister['re_pass']) {
					$message = "Mật khẩu không trùng khớp";
					$data['message'] = $message;
					return $this->load->view(self::AREA_FRONTEND . '/register', $data);
				}
				// check Email
				$checkEmail = $this->checkEmail($paramsRegister['email']);
				$urlRidect = 'register';
				if ($checkEmail) {
					$saveCustomer = $this->customer->createCustomer($paramsRegister);
					if ($saveCustomer) {
						$message = "Đã tạo tài khoản thành công";
						$urlRidect = 'login';
					} else {
						$message = "Tạo tài khoản không thành công";
					}
				} else {
					$message = "Email đã được sử dụng!";
					$urlRidect = 'register';
				}

				$data['message'] = $message;
				return $this->load->view(self::AREA_FRONTEND . '/' . $urlRidect, $data);
			}

		}
		return $this->load->view(self::AREA_FRONTEND . '/register');
	}

	public function login()
	{
		if ($this->session->userdata("userInfo")) {
			return redirect("home", "refresh");
		}
		$urlRidect = 'login';
		$message = "";
		$data = array();
		$login = false;
		if ($this->getRequest()) {
			$userInfo = $this->getRequest();
			if (isset($userInfo['email']) && isset($userInfo['password'])) {
//				if ($password && $this->checkPassWord($password)) {
				if ($userInfo['password']) {
					$userInfo['password'] = md5($userInfo['password']);
					$login = $this->customer->login($userInfo);
				}
			}

		}
		if ($login) {
			if (isset($login['password'])) {
				unset($login['password']);
			}
			$message = "Đăng nhập thành công";
			$userData = $login;
			$userData['logged_in'] = true;
			$this->session->set_userdata('userInfo', $userData);
			$urlRidect = "home";
			return redirect($urlRidect, "refresh");
		} elseif ($this->getRequest()) {
			$message = "Tài khoản hoặc mật khẩu không đúng";
		}
		$data["message"] = $message;
		return $this->load->view(self::AREA_FRONTEND . '/' . $urlRidect, $data);
	}

	/**
	 * @param array $paramsRegister
	 * @return boolean
	 */
	private function checkEmail($paramsRegister)
	{
		if ($paramsRegister) {
			return $this->customer->checkEmail($paramsRegister);
		}
		return false;

	}

	private function checkPassWord($password)
	{
		$check = false;
		//check password with regular expession
		if (preg_match("#.*^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9]).*$#", $password)) {
			$check = true;
		}

		return $check;
	}

	public function logout()
	{
			$userInfo = $this->session->userdata("userInfo");
			if ($userInfo) {
				$this->session->unset_userdata('userInfo');
				return redirect("login", "refresh");
			}
	}

}
