<?php


class Pusher extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
	}

	public function push(){
		$questionId = $this->input->post("question_id");
		$timeGame = $this->input->post("time_game");
		$this->load->view("vendor/autoload.php");
		$options = array(
			'cluster' => 'ap1',
			'useTLS' => true
		);
		$pusher = new Pusher\Pusher(
			'b9db9faad5a7b670f8d8',
			'b83fc1c0cefefd119b09',
			'939220',
			$options
		);

		$data['question_id'] = $questionId;
		$data['time_game'] = $timeGame;
		$data['start'] = true;
		$pusher->trigger('my-channel-kenzofunny-local', 'my-event-kenzofunny-local', $data);

	}

}
