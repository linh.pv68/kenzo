<?php


class Question extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->Model('QuestionModel');
		$this->load->Model('AnswerModel');
		$this->resultJson = array(
			'success' => true,
			'data' => array(),
			'error' => false
		);
	}

	public function index()
	{
		$data['questions'] = $this->getList();
		$this->load->view(self::AREA_BACKEND . '/question/view', $data);
	}

	public function add()
	{
		$result = array(
			'success' => true,
			'message' => '',
			'error' => false
		);

		if ($this->getRequest()) {
			$data = $this->getRequest();
			if (isset($_FILES["image_file"]["name"])) {
				$addImage = $this->addImage();
			}
			if ($addImage['success']) {
				$data['image'] = $addImage['data']['name'];
			}
			$question = $this->actionQuestion('add',$data);
			if ($question['success']) {
				$data['question_id'] = $question['data']['question_id'];
				$answer = $this->actionanswer('add',$data);
			}

			echo json_encode($result);


		} else {
			$this->load->view(self::AREA_BACKEND . '/question/add');
		}

		// Add question
	}

	public function edit($id)
	{
		$data = array();
		if ($id) {
			$question = $this->QuestionModel->getByIdQuestion($id);
			if ($question) {
				$data['question'] = $question;
				$data['option'] = $this->QuestionModel->getListOp();
				return $this->load->view(self::AREA_BACKEND . '/question/edit', $data);
			}

		}

	}

	public function sua()
	{
		if ($this->getRequest()) {
			$data =$this->getRequest();
			if (isset($_FILES["image_file"]["name"])) {
				$addImage = $this->addImage();
			}
			if ($addImage['success']) {
				$data['image'] = $addImage['data']['name'];
			}
			$question = $this->actionQuestion('edit',$data);
			if ($question['success']) {
				$answer = $this->actionanswer('edit',$data);
				header('Location:'. base_url(). 'question');
			}
			// print_r($data);
		}
	}

	public function delete(){
		$id = $this->input->post("id");
		try {
			$statusDelete = $this->QuestionModel->delete($id);
			if ($statusDelete) {
				$this->resultJson['success'] = true;
				$this->resultJson['data']['message'] = "Delete the code question success";
				$this->resultJson['error'] = false;
			} else {
				$this->resultJson['success'] = false;
				$this->resultJson['data']['message'] = "An error in process delete code question.";
				$this->resultJson['error'] = true;
			}

		} catch (Exception $exception) {
			$this->resultJson['success'] = false;
			$this->resultJson['data']['message'] = $exception->getMessage();
			$this->resultJson['error'] = true;
		}
		echo json_encode($this->resultJson);
	}

	private function addImage()
	{
		$result = array(
			'success' => false,
			'data' => array()
		);

		$config['upload_path'] = 'upload/images';
		$config['allowed_types'] = 'jpg|jpeg|png|gif';
		$this->upload->initialize($config);
		$this->load->library('upload', $config);
		if (!$this->upload->do_upload('image_file')) {
//			echo $this->upload->display_errors();
			$result['data']['message'] = 'An error when upload Image.';

		} else {
			$result['success'] = true;
			$result['data']['name'] = $this->upload->data('file_name');
		}

		return $result;
	}

	private function actionanswer($method,$data)
	{
		$valueAnswer = array();
		foreach ($data as $key => $value) {
			switch ($key) {
				case "answer1":
					$valueAnswer['answer1'] = $value;
					break;
				case "answer2":
					$valueAnswer['answer2'] = $value;
					break;
				case "answer3":
					$valueAnswer['answer3'] = $value;
					break;
				case "answer4":
					$valueAnswer['answer4'] = $value;
					break;
				case "correct":
					$valueAnswer['correct'] = $value;
					break;
				case "question_id":
					$valueAnswer['question_id'] = $value;
					break;
			}
		}
		switch ($method) {
				case 'add':
					$statusQuery = $this->AnswerModel->add($valueAnswer);
					break;
				case 'edit':
					$q_id = $data['id'];
					$statusQuery = $this->AnswerModel->edit($q_id,$valueAnswer);
					break;
		}

	}

	private function actionQuestion($method,$data)
	{
		$result = array(
			'success' => false,
			'data' => ''
		);
		$valueQuestion = array(
			'content' => '',
			'des' => '',
			'code_id' => '',
			'image' => '',

		);

		foreach ($data as $key => $value) {
			switch ($key) {
				case "content_q":
					$valueQuestion['content'] = $value;
					break;
				case "des":
					$valueQuestion['des'] = $value;
					break;
				case "code_id":
					$valueQuestion['code_id'] = $value;
					break;
				case "image":
					$valueQuestion['image'] = $value;
					break;
			}
		}
		switch ($method) {
				case 'add':
					$statusQuery = $this->QuestionModel->add($valueQuestion);
					break;
				case 'edit':
					$q_id = $data['id'];
					$statusQuery = $this->QuestionModel->edit($q_id,$valueQuestion);
					break;
		}
		if ($statusQuery['success']) {
			$result['success'] = true;
			$result['data'] = array(
				'question_id' => $statusQuery['question_id']
			);
		} else {
			$result['message'] = 'An Error when add question.';
		}
		return $result;
	}


	public function getList()
	{
		$questions = $this->QuestionModel->getList();
		if ($questions) {
			foreach ($questions as $key => $value) {
				if (isset($value['image'])) {
					if ($value['image']){
						$questions[$key]['image'] = base_url() . 'upload/images/' . $value['image'];
					}else{
						$questions[$key]['image'] = base_url() . 'upload/images/image-default.png';
					}
				}
			}
			return $questions;
		}
		return false;
	}


}
