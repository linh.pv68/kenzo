<?php


class Scores extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->model("AnswerCustomerModel");
		$this->load->model("QuestionModel");
		$this->load->model("ScoresCustomerModel");
		$this->load->Model("CodeQuestionModel");
	}

	public function index()
	{

	}

	public function getChartsScores()
	{
		$result = array(
			'success' => false,
			'data' => array(),
			'error' => true
		);
		if ($this->getRequest()) {
			$params = $this->getRequest();
			$setChartsScores = $this->setChartsScores($params);
			if ($setChartsScores) {
				$chartsScores = $this->ScoresCustomerModel->getByCodeQuestionId($params['codequestion_id']);
				$result['success'] = true;
				$result['data'] = $chartsScores;
				$result['error'] = false;
			} else {
				$result['data']['message'] = "Lỗi trong quá trình tính điểm";
			}
		} else {
			$result['data']['message'] = "Lỗi trong quá trình tính điểm";
		}

		echo json_encode($result);
	}

	protected function setChartsScores($params)
	{
		try {
			if (isset($params['codequestion_id']) && isset($params['question_id'])) {
				$question_id = $params['question_id'];
				$codeQuestionId = $params['codequestion_id'];
				$answerCustomer = $this->AnswerCustomerModel->getByQuestionId($codeQuestionId, $question_id);
				$questionInfo = $this->QuestionModel->getById($params['question_id']);

				if (isset($questionInfo['correct'])) {
					foreach ($answerCustomer as $value) {
						$scores_customer = [
							"user_id" => $value['user_id'],
							"codequestion_id" => $value['codequestion_id']
						];
						if ($value['question_id'] == $question_id) {
							$this->scoresCustomer('insert', $scores_customer);
						}
						if ($value['question_id'] == $question_id && $value['answer'] == $questionInfo['correct']) {
							$this->scoresCustomer('increase', $scores_customer);

						}
						/*else {
							$this->scoresCustomer('decrease', $scores_customer);
						}*/
					}

				}
			}
			return true;
		} catch (Exception $exception) {
			return false;
		}

	}

	private function scoresCustomer($action, $data)
	{
		$scoresCustomerData = [
			'user_id' => $data['user_id'],
			'codequestion_id' => $data['codequestion_id'],
			'scores' => 0,
			'total_correct' => 0
		];
		$scoreCustomerInfo = $this->ScoresCustomerModel->checkExist($data);

		if ($scoreCustomerInfo) {
			$scoresCustomerData = $scoreCustomerInfo;
		} else {
			if ($action == 'insert') {
				$this->ScoresCustomerModel->add($scoresCustomerData);
				return true;
			}
		}
		switch ($action) {
			case 'increase':
				$scoresCustomerData['scores'] += 10;
				$scoresCustomerData['total_correct'] += 1;
				break;
			case  'decrease':
				$scoresCustomerData['scores'] -= 10;
				if ($scoresCustomerData['total_correct'] != 0) {
					$scoresCustomerData['total_correct'] -= 1;
				}
				break;
		}
		try {
			$this->ScoresCustomerModel->add($scoresCustomerData);
			return true;
		} catch (Exception $exception) {
			return false;
		}

	}

	public function getCharts()
	{
		if ($code_id = $this->input->post('code_id')) {
			$chartsScores = $this->ScoresCustomerModel->getByCodeQuestionId($code_id);
			$totalQuestion = $this->QuestionModel->getByCodeId($code_id);
			if ($totalQuestion) {
				$totalQuestion = sizeOf($totalQuestion);
			} else {
				$totalQuestion = 0;
			}
			$this->load->view(self::AREA_BACKEND . '/view_charts', array('charts' => $chartsScores, 'total_question' => $totalQuestion));
		} else {
			$pinCodes = $this->CodeQuestionModel->getList();
			$data = array();
			if ($pinCodes) {
				foreach ($pinCodes as $value) {
					if (isset($value['code'])) {
						$code = $value['code'];
					}
					if (isset($value['title'])) {
						$title = $value['title'];
					}
					if (isset($value['id'])) {
						$id = $value['id'];
					}
					if (isset($code) && isset($title) && isset($id)) {
						$data['pin_codes'][] = array(
							"id" => $id,
							"code" => $code,
							"title" => $title
						);
					}


				}
			}
			$this->load->view(self::AREA_BACKEND . '/charts', $data);
		}
	}

	public function resetScore()
	{
		$result = array(
			'success' => false,
			'data' => array(),
			'error' => true
		);

		$codeQuestionId = $this->input->post("code_question_id");
		if ($codeQuestionId) {
			try {
				$this->ScoresCustomerModel->deleteByCodeQuestionId($codeQuestionId);
				$this->AnswerCustomerModel->deleteByCodeQuestionId($codeQuestionId);
				$result['success'] = true;
				$result['error'] = false;

			} catch (Exception $exception) {
				$result['data']['message'] = $exception->getMessage();
			}
		} else {
			$result['data']['message'] = "Lỗi trong quá trình reset điểm.";
		}
		echo json_encode($result);
	}

	private function getTotalQuestion($questionId)
	{

	}
}
