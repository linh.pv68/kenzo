<?php


class AnswerCustomerModel extends CI_Model
{
	const TableDB = "answer_customer";

	public function __construct()
	{
		$this->load->database();
	}


	public function add($data)
	{
		$resultInsertQuery = array(
			'success' => false,
			'id' => ''
		);
		if ($data) {
			$insert = $this->db->insert(self::TableDB, $data);
			if ($insert) {
				$id = $this->db->insert_id();
				$resultInsertQuery['success'] = $insert;
				$resultInsertQuery['id'] = $id;
			}
		}
		return $resultInsertQuery;
	}

	public function getByQuestionId($codeQuestionId, $question_id)
	{
		$this->db->where("codequestion_id", $codeQuestionId);
		$this->db->where("question_id", $question_id);
		$query = $this->db->get(self::TableDB);
		if ($query) {
			return $query->result_array();
		} else {
			return false;
		}

	}

	public function deleteByCodeQuestionId($codeQuestionId)
	{
		$this->db->where("codequestion_id", $codeQuestionId);
		return $this->db->delete(self::TableDB);
	}

}
