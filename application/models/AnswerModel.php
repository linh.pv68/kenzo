<?php


class AnswerModel extends CI_Model
{
	const TableDB = "answer";

	public function __construct()
	{
		$this->load->database();
	}


	public function add($data)
	{
		$resultInsertQuery = array(
			'success' => false,
			'answer_id' => ''
		);
		if ($data) {
			$insert = $this->db->insert(self::TableDB, $data);
			if ($insert) {
				$answerId = $this->db->insert_id();
				$resultInsertQuery['success'] = $insert;
				$resultInsertQuery['answer_id'] = $answerId;
			}
		}
		return $resultInsertQuery;

		// TODO: Implement add() method.
	}

	public function delete($data)
	{
		// TODO: Implement delete() method.
	}

	public function edit($id,$data)
	{
		$resultUpdateQuery = array(
			'success' => false,
		);
		if ($data) {
			$this->db->where("question_id", $id);
			$update = $this->db->update(self::TableDB, $data);
			if ($update) {
				$resultUpdateQuery['success'] = $update;
			}
		}
		return $resultUpdateQuery;
		// TODO: Implement edit() method.
	}
}
