<?php

class CodeQuestionModel extends CI_Model
{
	const TableDB = "codequestion";

	public function __construct()
	{
		parent::__construct();
		$this->load->database();
	}

	public function add($data)
	{
		if ($data) {
			return $this->db->insert(self::TableDB, $data);
		}
		// TODO: Implement delete() method.
	}

	public function delete($data)
	{
		$this->db->where("id", $data);
		return $this->db->delete(self::TableDB);


		// TODO: Implement delete() method.
	}

	public function edit($data)
	{
		$this->db->where("id", $data['id']);
		if ($this->db->update(self::TableDB, $data))
			return true;
		else
			return false;
		// TODO: Implement edit() method.
	}

	public function getList()
	{
		$query = $this->db->get(self::TableDB);
		if ($query) {
			return $query->result_array();
		} else {
			return false;
		}
		// TODO: Implement getList() method.
	}

	public function getById($id){
		$this->db->where("id",$id);
		$query = $this->db->get(self::TableDB);
		if ($query) {
			return $query->row_array();
		}
		else
		{
			return false;
		}
	}

	public function checkPinCode($pinCode){
		$this->db->select('*');
		$this->db->where("code",$pinCode);
		$query = $this->db->get(self::TableDB);
		if ($query) {
			return $query->row_array();
		}
		else
		{
			return false;
		}
	}
}
