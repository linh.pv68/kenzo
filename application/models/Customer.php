<?php


class Customer extends CI_Model
{
	public function __construct()
	{
		$this->load->database();
	}

	/**
	 * @param array $data
	 * @return mixed
	 */
	public function createCustomer($data)
	{
		if ($data) {
			if (isset($data['password'])) {
				$data['password'] = md5($data['password']);
				unset($data['re_pass']);
			}
			return $this->db->insert('customer', $data);
		}
	}

	/**
	 * @param string $email
	 * @return boolean
	 * */
	public function checkEmail($email)
	{
		$this->db->select('email, user_id');
		$array = array('email' => $email);
		$this->db->where($array);
		$result = $this->db->get('customer')->num_rows();
		$checkEmail = true;
		if ($result > 0) {
			$checkEmail = false;
		}
		return $checkEmail;
	}

	/**
	 * @param array $userInformation
	 * @return mixed
	 */
	public function login($userInformation)
	{
		$this->db->select('*');
		$array = array('email' => $userInformation['email'], 'password' => $userInformation['password']);
		$this->db->where($array);
		return $this->db->get('customer')->row_array();
	}

	public function getList()
	{
		$this->db->select('*');
		return $this->db->get('customer')->result_array();
	}

	public function delete($userId)
	{
		$this->db->where("user_id", $userId);
		return $this->db->delete("customer");
	}

}
