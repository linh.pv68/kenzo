<?php 
 	class Madmin extends CI_Model{
	    public function __construct(){
	      parent::__construct();
			$this->load->database();
	    }
	    //insert 
	    public function add_article($table,$add){
	      if ($this->db->insert($table, $add)){
	        return true;
	      }
	      else {
	        return false;
	      }
	    }

	    // select
	    public function get_multi($table){
	    	$query = $this->db->get($table);
	    	if ($query) {
	    		return $query->result();
	    	}
	    	else{
	    		return false;
	    	}
	    }

	       // get  by id
	    function get_by_id($table,$id){
	      $this->db->where("id",$id);
	      $query = $this->db->get($table);
	      if ($query) {
	        return $query->row_array();
	      }
	      else
	      {
	        return false;
	      }
	    }

	    function show_list_data($data){
			$output ='';
			foreach ($data as $key => $value) {
				$k=$key+1;
				$output.='
					<tr>
						<td>'.$k.'</td>
						<td>'.$value->code.'</td>
						<td>'.$value->title.'</td>
						<td>'.$value->des.'</td>
						<td>
							<button data-id="'.$value->id.'" class="btn btn-warning edit" style="padding: 6px 4px; margin-right:4px;">Sửa</button>
							<button data-id="'.$value->id.'" class="btn btn-danger del" style="padding: 6px 4px; margin-right:4px;">Xóa</button>
						</td>
					</tr>
				';
				
			}
			echo $output;
		}

		function del_data($id,$table)
	    {
	      $this->db->where("id", $id);
	      $this->db->delete($table);
	    }


	        //update 
	    function update_data($data,$id,$table){
	      $this->db->where("id",$id);
	      if($this->db->update($table, $data))
	        return true;
	      else
	        return false;
	    }

	    
	     public function do_upload($image_name)
	    {
	      die($image_name);
	      $image_path = realpath(FCPATH. "/uploads/images");
	      $config = array(
	        'allowed_types' => 'jpg|jpeg|gif|png',
	        'upload_path' => $image_path
	      );
	      $this->load->library('upload' , $config);
	      if (!$this->upload->do_upload($image_name))
	      {
	        $error = array('error' => $this->upload->display_errors());
	        die($this->upload->display_errors());
	      }
	      else
	      {
	        $image_data = $this->upload->data();
	        return $image_data['file_name'];
	      }
	    }

	    public function last_roll_fetch(){
       	 	return $this->db->query("SELECT * FROM question ORDER BY id  DESC LIMIT 1")->result();

    	} 

	}

 ?>
