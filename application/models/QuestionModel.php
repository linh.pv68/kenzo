<?php


class QuestionModel extends CI_Model
{
	const TableDB = "question";

	public function __construct()
	{
		$this->load->database();
	}


	public function add($data)
	{
		$resultInsertQuery = array(
			'success' => false,
			'question_id' => ''
		);
		if ($data) {
			$insert = $this->db->insert(self::TableDB, $data);
			if ($insert) {
				$questionId = $this->db->insert_id();
				$resultInsertQuery['success'] = $insert;
				$resultInsertQuery['question_id'] = $questionId;
			}
		}
		return $resultInsertQuery;

		// TODO: Implement add() method.
	}

	public function delete($data)
	{
		$this->db->where("id", $data);
		return $this->db->delete(self::TableDB);


		// TODO: Implement delete() method.
	}

	public function edit($q_id,$data)
	{
		$resultUpdateQuery = array(
			'success' => false,
			'question_id' => $q_id
		);
		if ($data) {
			$this->db->where("id", $q_id);
			$update = $this->db->update(self::TableDB, $data);
			if ($update) {
				$resultUpdateQuery['success'] = $update;
			}
		}
		return $resultUpdateQuery;
		// TODO: Implement edit() method.
	}

	public function getByIdQuestion($id)
	{
		$this->db->select("qu.id, qu.des, qu.content , qu.image, qu.code_id, coqu.code, coqu.title, an.question_id, an.answer1,an.answer2,an.answer3,an.answer4,an.correct");
		$this->db->where("qu.id", $id);
		$this->db->from(self::TableDB . ' as qu');
		$this->db->join('codequestion as coqu', 'coqu.id = qu.code_id');
		$this->db->join('answer as an', 'an.question_id = qu.id');
		$query = $this->db->get();
		if ($query) {
			return $query->row_array();
		} else {
			return false;
		}
	}

	public function getById($id)
	{
		$this->db->select("*");
		$this->db->select("coqu.id as code_id", FALSE);
		$this->db->select("an.id as answer_id", FALSE);
		$this->db->select("coqu.des as code_des", FALSE);
		$this->db->where("qu.id", $id);
		$this->db->from(self::TableDB . ' as qu');
		$this->db->join('codequestion as coqu', 'coqu.id = qu.code_id');
		$this->db->join('answer as an', 'an.question_id = qu.id');
		$query = $this->db->get();
		if ($query) {
			return $query->row_array();
		} else {
			return false;
		}

	}

	public function getListOp()
	{
		$this->db->select("*");
		$this->db->from('codequestion');
		$query = $this->db->get();
		if ($query) {
			return $query->result_array();
		} else {
			return false;
		}
	}

	public function getList()
	{
		$this->db->select('qu.id, qu.des, qu.content , qu.image, coqu.code');
		$this->db->from(self::TableDB . ' as qu');
		$this->db->join('codequestion as coqu', 'coqu.id = qu.code_id');
		$query = $this->db->get();
		if ($query) {
			return $query->result_array();
		} else {
			return false;
		}
		// TODO: Implement getList() method.
	}

	public function getByCodeId($codeId){
		$this->db->select('*');
		$this->db->where("code_id", $codeId);
		$this->db->from(self::TableDB);
		$query = $this->db->get();
		if ($query) {
			return $query->result_array();
		} else {
			return false;
		}
	}
}
