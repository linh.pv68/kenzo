<?php


class ScoresCustomerModel extends CI_Model
{
	const TableDB = "scores_customer";

	public function __construct()
	{
		$this->load->database();
	}

	public function add($data)
	{
		$resultInsertQuery = [
			'success' => false,
			'id' => ''
		];

		if ($data) {
			if (isset($data['id'])) {
				$id = $data['id'];
				unset($data['id']);
				$this->db->where('id', $id);
				$this->db->update(self::TableDB, $data);
				return true;
			} else {
				$insert = $this->db->insert(self::TableDB, $data);

			}
			if ($insert) {
				$id = $this->db->insert_id();
				$resultInsertQuery['success'] = $insert;
				$resultInsertQuery['id'] = $id;
			}
		}
		return $resultInsertQuery;
	}

	public function checkExist($data)
	{
		$this->db->where('user_id', $data['user_id']);
		$this->db->where('codequestion_id', $data['codequestion_id']);
		$query = $this->db->get(self::TableDB);
		if ($query) {
			return $query->row_array();
		} else {
			return false;
		}
	}

	public function getByCodeQuestionId($codeQuestionId)
	{
		$this->db->select("*");
		$this->db->from(self::TableDB);
		$this->db->join('customer', 'customer.user_id = scores_customer.user_id');
		$this->db->where("codequestion_id", $codeQuestionId);
		$this->db->order_by('scores', 'DESC');
		$query = $this->db->get();
		if ($query) {
			return $query->result_array();
		} else {
			return false;
		}
	}

	public function deleteByCodeQuestionId($codeQuestionId){
		$this->db->where("codequestion_id", $codeQuestionId);
		return $this->db->delete(self::TableDB);
	}

}
