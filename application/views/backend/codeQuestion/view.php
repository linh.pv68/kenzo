<?php
if (!$this->session->userdata("userAdminInfo")) {
	return redirect(base_url() . 'admin/login');
}
?>

<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>Kenzo Funny | Dashboard</title>
	<!-- Tell the browser to be responsive to screen width -->
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="icon" type="image/png" href="/templates/images/favicon.png"/>
	<!-- Font Awesome -->
	<link rel="stylesheet" href="/templates/admin/plugins/fontawesome-free/css/all.min.css">
	<!-- Ionicons -->
	<link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
	<!-- Tempusdominus Bbootstrap 4 -->
	<link rel="stylesheet"
		  href="/templates/admin/plugins/tempusdominus-bootstrap-4/css/tempusdominus-bootstrap-4.min.css">
	<!-- iCheck -->
	<link rel="stylesheet" href="/templates/admin/plugins/icheck-bootstrap/icheck-bootstrap.min.css">
	<!-- JQVMap -->
	<link rel="stylesheet" href="/templates/admin/plugins/jqvmap/jqvmap.min.css">
	<!-- Theme style -->
	<link rel="stylesheet" href="/templates/admin/dist/css/adminlte.min.css">
	<!-- overlayScrollbars -->
	<link rel="stylesheet" href="/templates/admin/plugins/overlayScrollbars/css/OverlayScrollbars.min.css">
	<!-- Daterange picker -->
	<link rel="stylesheet" href="/templates/admin/plugins/daterangepicker/daterangepicker.css">
	<!-- summernote -->
	<link rel="stylesheet" href="/templates/admin/plugins/summernote/summernote-bs4.css">
	<!-- Google Font: Source Sans Pro -->
	<link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
</head>
<body class="hold-transition sidebar-mini layout-fixed">
<div class="wrapper">

	<!-- Navbar -->
	<?php $this->load->view("backend/navbar"); ?>
	<!-- /.navbar -->

	<!-- Main Sidebar Container -->
	<aside class="main-sidebar sidebar-dark-primary elevation-4">
		<?php $this->load->view("backend/sider_bar"); ?>
		<!-- /.sidebar -->
	</aside>

	<!-- Content Wrapper. Contains page content -->
	<div class="content-wrapper">
		<!-- Content Header (Page header) -->
		<?php $this->load->view("backend/header"); ?>
		<!-- /.content-header -->

		<!-- Main content -->
		<section class="content">
			<div class="container-fluid">
				<!-- Page Content  -->
				<div id="content">
					<?php echo validation_errors(); ?>
					<h2>Thêm bộ câu hỏi</h2>
					<form class="form-inline" method="POST">

						<div class="form-group" style="margin-right:20px;">
							<label for="" style="margin-right:10px;">Mã</label>
							<input type="number" class="form-control" id="maBoCauHoi" placeholder="" name="maBoCauHoi">
						</div>
						<div class="form-group" style="margin-right:20px;">
							<label for="" style="margin-right:10px;">Tên</label>
							<input type="text" class="form-control" id="tenBoCauHoi" placeholder="" name="tenBoCauHoi">
						</div>
						<div class="form-group" style="margin-right:20px;">
							<label for="" style="margin-right:10px;">Mô tả</label>
							<input type="text" class="form-control" id="moTaBoCauHoi" placeholder="">
						</div>
						<button type="button" class="btn btn-info" id="addData">Thêm</button>
						<button type="button" class="btn btn-info" id="editData" style="display: none;">
							<span id="getId" style="display:none"></span>Sửa
						</button>
					</form>

					<div class="line"></div>

					<h2>Danh sách bộ câu hỏi</h2>
					<div class="container">
						<table class="table">
							<thead>
							<tr>
								<th>ID</th>
								<th>Mã bộ câu hỏi</th>
								<th>Tên bộ câu hỏi</th>
								<th>Mô tả</th>
								<th>Tác vụ</th>
							</tr>
							</thead>
							<tbody id="list">

							</tbody>
						</table>
					</div>

				</div>
			</div><!-- /.container-fluid -->
		</section>
		<!-- /.content -->
	</div>
	<!-- /.content-wrapper -->
	<?php $this->load->view("backend/footer"); ?>
	<!-- /.control-sidebar -->
</div>
<!-- ./wrapper -->

<!-- jQuery -->
<script src="/templates/admin/plugins/jquery/jquery.min.js"></script>
<!-- jQuery UI 1.11.4 -->
<script src="/templates/admin/plugins/jquery-ui/jquery-ui.min.js"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
	$.widget.bridge('uibutton', $.ui.button)
</script>
<!-- Bootstrap 4 -->
<script src="/templates/admin/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- overlayScrollbars -->
<script src="/templates/admin/plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js"></script>
<!-- AdminLTE App -->
<script src="/templates/admin/dist/js/adminlte.js"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<script src="/templates/admin/dist/js/pages/dashboard.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="/templates/admin/dist/js/demo.js"></script>
<script type="text/javascript" src="<?php echo base_url("templates/js/code-question.js"); ?>"></script>

</body>
</html>


