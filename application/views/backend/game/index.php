<?php
if (!$this->session->userdata("userAdminInfo")) {
	return redirect(base_url() . 'admin/login');
}
?>

<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>Kenzo Funny | Dashboard</title>
	<!-- Tell the browser to be responsive to screen width -->
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="icon" type="image/png" href="/templates/images/favicon.png"/>
	<!-- Font Awesome -->
	<link rel="stylesheet" href="/templates/admin/plugins/fontawesome-free/css/all.min.css">
	<!-- Ionicons -->
	<link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
	<!-- Tempusdominus Bbootstrap 4 -->
	<link rel="stylesheet"
		  href="/templates/admin/plugins/tempusdominus-bootstrap-4/css/tempusdominus-bootstrap-4.min.css">
	<!-- iCheck -->
	<link rel="stylesheet" href="/templates/admin/plugins/icheck-bootstrap/icheck-bootstrap.min.css">
	<!-- JQVMap -->
	<link rel="stylesheet" href="/templates/admin/plugins/jqvmap/jqvmap.min.css">
	<!-- Theme style -->
	<link rel="stylesheet" href="/templates/admin/dist/css/adminlte.min.css">
	<!-- overlayScrollbars -->
	<link rel="stylesheet" href="/templates/admin/plugins/overlayScrollbars/css/OverlayScrollbars.min.css">
	<!-- Daterange picker -->
	<link rel="stylesheet" href="/templates/admin/plugins/daterangepicker/daterangepicker.css">
	<!-- summernote -->
	<link rel="stylesheet" href="/templates/admin/plugins/summernote/summernote-bs4.css">
	<link rel="stylesheet" href="/templates/admin/css/dashboard.css">
	<!-- Google Font: Source Sans Pro -->
	<link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
</head>
<body class="hold-transition sidebar-mini layout-fixed">
<div class="wrapper">

	<!-- Navbar -->
	<?php $this->load->view("backend/navbar"); ?>
	<!-- /.navbar -->

	<!-- Main Sidebar Container -->
	<aside class="main-sidebar sidebar-dark-primary elevation-4">
		<?php $this->load->view("backend/sider_bar"); ?>
		<!-- /.sidebar -->
	</aside>

	<!-- Content Wrapper. Contains page content -->
	<div class="content-wrapper">
		<!-- Content Header (Page header) -->
		<?php $this->load->view("backend/header"); ?>
		<!-- /.content-header -->
		<!-- Main content -->
		<section class="content">
			<div class="container-fluid">
				<div class="code-title" style="font-size: 24px;font-weight: 600;float: left;">Bộ câu Hỏi</div>
				<div style="float: right;margin-bottom: 15px;">
					<a class="btn btn-danger reset-scores" href="#">Reset Điểm</a>
				</div>
				<table class="table">
					<thead>
					<tr>
						<th>Thứ Tự</th>
						<th>Mô tả</th>
						<th>Tác vụ</th>
					</tr>
					</thead>
					<tbody>
					<?php if (isset($list_question)): ?>
						<?php foreach ($list_question as $key => $value): ?>
							<?php $key = $key + 1 ?>
							<tr>
								<td><?= "Câu Hỏi " . $key ?></td>
								<td><?= $value['des'] ?></td>
								<td>
									<a href="<?= base_url('game/play/') . $value['id'] ?>"
									   class="btn btn-primary" style="padding: 4px 6px;">
										Chơi
									</a>
								</td>
							</tr>
						<?php endforeach; ?>

					<?php endif; ?>
					</tbody>
				</table>
			</div>
	</div><!-- /.container-fluid -->
	</section>
	<!-- /.content -->
</div>
<!-- /.content-wrapper -->
<?php $this->load->view("backend/footer"); ?>
<!-- /.control-sidebar -->
</div>
<!-- ./wrapper -->

<!-- jQuery -->
<script src="/templates/admin/plugins/jquery/jquery.min.js"></script>
<!-- jQuery UI 1.11.4 -->
<script src="/templates/admin/plugins/jquery-ui/jquery-ui.min.js"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
	$.widget.bridge('uibutton', $.ui.button)
</script>
<!-- Bootstrap 4 -->
<script src="/templates/admin/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- ChartJS -->
<script src="/templates/admin/plugins/chart.js/Chart.min.js"></script>
<!-- Sparkline -->
<script src="/templates/admin/plugins/sparklines/sparkline.js"></script>
<!-- jQuery Knob Chart -->
<script src="/templates/admin/plugins/jquery-knob/jquery.knob.min.js"></script>
<!-- daterangepicker -->
<script src="/templates/admin/plugins/moment/moment.min.js"></script>
<script src="/templates/admin/plugins/daterangepicker/daterangepicker.js"></script>
<!-- Tempusdominus Bootstrap 4 -->
<script src="/templates/admin/plugins/tempusdominus-bootstrap-4/js/tempusdominus-bootstrap-4.min.js"></script>
<!-- Summernote -->
<script src="/templates/admin/plugins/summernote/summernote-bs4.min.js"></script>
<!-- overlayScrollbars -->
<script src="/templates/admin/plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js"></script>
<!-- AdminLTE App -->
<script src="/templates/admin/dist/js/adminlte.js"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<!--<script src="/templates/admin/dist/js/pages/dashboard.js"></script>-->
<!-- AdminLTE for demo purposes -->
<script src="/templates/admin/dist/js/demo.js"></script>

<script src="https://js.pusher.com/5.1/pusher.min.js"></script>

<script src="/templates/admin/js/dashboard.js"></script>
</body>
</html>

