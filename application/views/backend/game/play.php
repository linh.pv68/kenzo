<?php
if (!$this->session->userdata("userAdminInfo")) {
	return redirect(base_url() . 'admin/login');
}
?>

<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>Kenzo Funny | Dashboard</title>
	<!-- Tell the browser to be responsive to screen width -->
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="icon" type="image/png" href="/templates/images/favicon.png"/>
	<!-- Font Awesome -->
	<link rel="stylesheet" href="/templates/admin/plugins/fontawesome-free/css/all.min.css">
	<!-- Ionicons -->
	<link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
	<!-- Tempusdominus Bbootstrap 4 -->
	<link rel="stylesheet"
		  href="/templates/admin/plugins/tempusdominus-bootstrap-4/css/tempusdominus-bootstrap-4.min.css">
	<!-- iCheck -->
	<link rel="stylesheet" href="/templates/admin/plugins/icheck-bootstrap/icheck-bootstrap.min.css">
	<!-- JQVMap -->
	<link rel="stylesheet" href="/templates/admin/plugins/jqvmap/jqvmap.min.css">
	<!-- Theme style -->
	<link rel="stylesheet" href="/templates/admin/dist/css/adminlte.min.css">
	<!-- overlayScrollbars -->
	<link rel="stylesheet" href="/templates/admin/plugins/overlayScrollbars/css/OverlayScrollbars.min.css">
	<!-- Daterange picker -->
	<link rel="stylesheet" href="/templates/admin/plugins/daterangepicker/daterangepicker.css">
	<!-- summernote -->
	<link rel="stylesheet" href="/templates/admin/plugins/summernote/summernote-bs4.css">
	<link rel="stylesheet" href="/templates/admin/css/dashboard.css">
	<link rel="stylesheet" href="/templates/admin/css/game.css">
	<!-- Google Font: Source Sans Pro -->
	<link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
</head>
<body class="hold-transition sidebar-mini layout-fixed">
<div class="wrapper">

	<!-- Navbar -->
	<?php $this->load->view("backend/navbar"); ?>
	<!-- /.navbar -->

	<!-- Main Sidebar Container -->
	<aside class="main-sidebar sidebar-dark-primary elevation-4">
		<?php $this->load->view("backend/sider_bar"); ?>
		<!-- /.sidebar -->
	</aside>

	<!-- Content Wrapper. Contains page content -->
	<div class="content-wrapper">
		<!-- Content Header (Page header) -->
		<?php $this->load->view("backend/header"); ?>
		<!-- /.content-header -->
		<!-- Main content -->
		<section class="content">
			<div class="container-fluid">
				<!-- Modal Chart Scores -->
				<div class="modal fade" id="modalChartScores" tabindex="-1" role="dialog" aria-hidden="true">
					<div class="modal-dialog modal-dialog-centered" role="document">
						<div class="modal-content">
							<div class="modal-header">
								<h5 class="modal-title" id="modalChartScoresTitle">Bảng Xếp Hạng</h5>
								<button type="button" class="close" data-dismiss="modal" aria-label="Close">
									<span aria-hidden="true">&times;</span>
								</button>
							</div>
							<div class="modal-body">
								<div class="card-body p-0">
									<ul class="products-list product-list-in-card pl-2 pr-2" id="contentChartScores">
										<li class="item">
											<div class="product-img">1</div>
											<div class="product-info">
												<a href="javascript:void(0)" class="product-title">Phùng Văn Linh<span class="badge badge-warning float-right">100đ</span></a>
												<span class="product-description">518161</span>
											</div>
										</li>
										<!-- /.item -->
									</ul>
								</div>
							</div>
							<div class="modal-footer">
								<button onclick="document.getElementById('myform').submit();" type="button" class="btn btn-primary" data-dismiss="modal">OK</button>
							</div>
						</div>
					</div>
				</div>
				<?php if (isset($question)): ?>
					<div class="head-game">
						<span style="font-size: 26px;font-weight: 600;">Câu Hỏi: </span>
						<p style="font-size: 24px;"><?= $question['content'] ?></p>
					</div>
					<div class="info-game">
						<span class="time-game">30s</span>
						<?php if ($question['image']): ?>
							<img src="<?= base_url('upload/images/') . $question['image'] ?>">
						<?php else: ?>
							<img src="<?= base_url('upload/images/image-default.png') ?>">
						<?php endif; ?>
					</div>

					<div class="content-game">
						<div class="container">
							<div class="row">
								<div class="col">
									<button id="anwsera" class="btn btn-primary btn-lg btn-block">
										A. <?= $question['answer1'] ?></button>
								</div>
								<div class="col">
									<button id="anwserb" class="btn btn-secondary btn-lg btn-block">
										B. <?= $question['answer2'] ?></button>
								</div>
								<div class="w-100"></div>
								<div class="col">
									<button id="anwserc" class="btn btn-success btn-lg btn-block">
										C. <?= $question['answer3'] ?></button>
								</div>
								<div class="col">
									<button id="anwserd" class="btn btn-danger btn-lg btn-block">
										D. <?= $question['answer4'] ?></button>
								</div>
							</div>
						</div>
					</div>
					<div class="foot-game">
						<form id="myform" method="post" action="<?= base_url('game/index') ?>">
							<input hidden name="code_id" value="<?= $question['code_id'] ?>">
						</form>
						<input id="question_id" value="<?= $question['id'] ?>" hidden/>
						<input id="answer_correct" value="<?= $question['correct'] ?>" hidden/>
						<button onclick="document.getElementById('myform').submit();">Trở lại</button>
						<button id="start-game">Bắt Đầu Tính Giờ</button>
					</div>
				<?php endif; ?>

			</div>
	</div><!-- /.container-fluid -->
	</section>
	<!-- /.content -->
</div>
<!-- /.content-wrapper -->
<?php $this->load->view("backend/footer"); ?>
<!-- /.control-sidebar -->
</div>
<!-- ./wrapper -->

<!-- jQuery -->
<script src="/templates/admin/plugins/jquery/jquery.min.js"></script>
<!-- jQuery UI 1.11.4 -->
<script src="/templates/admin/plugins/jquery-ui/jquery-ui.min.js"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
	$.widget.bridge('uibutton', $.ui.button)
</script>
<!-- Bootstrap 4 -->
<script src="/templates/admin/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- ChartJS -->
<script src="/templates/admin/plugins/chart.js/Chart.min.js"></script>
<!-- Sparkline -->
<script src="/templates/admin/plugins/sparklines/sparkline.js"></script>
<!-- jQuery Knob Chart -->
<script src="/templates/admin/plugins/jquery-knob/jquery.knob.min.js"></script>
<!-- daterangepicker -->
<script src="/templates/admin/plugins/moment/moment.min.js"></script>
<script src="/templates/admin/plugins/daterangepicker/daterangepicker.js"></script>
<!-- Tempusdominus Bootstrap 4 -->
<script src="/templates/admin/plugins/tempusdominus-bootstrap-4/js/tempusdominus-bootstrap-4.min.js"></script>
<!-- Summernote -->
<script src="/templates/admin/plugins/summernote/summernote-bs4.min.js"></script>
<!-- overlayScrollbars -->
<script src="/templates/admin/plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js"></script>
<!-- AdminLTE App -->
<script src="/templates/admin/dist/js/adminlte.js"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<!--<script src="/templates/admin/dist/js/pages/dashboard.js"></script>-->
<!-- AdminLTE for demo purposes -->
<script src="/templates/admin/dist/js/demo.js"></script>

<script src="https://js.pusher.com/5.1/pusher.min.js"></script>

<script src="/templates/admin/js/game.js"></script>
</body>
</html>

