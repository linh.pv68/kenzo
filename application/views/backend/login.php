<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>Admin | Đăng nhập</title>
	<!-- Tell the browser to be responsive to screen width -->
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="icon" type="image/png" href="/templates/images/favicon.png"/>
	<link rel="icon" type="image/png" href="/templates/images/favicon.png"/>

	<!-- Font Awesome -->
	<link rel="stylesheet" href="/templates/admin/plugins/fontawesome-free/css/all.min.css">
	<!-- icheck bootstrap -->
	<link rel="stylesheet" href="/templates/admin/plugins/icheck-bootstrap/icheck-bootstrap.min.css">
	<!-- Theme style -->
	<link rel="stylesheet" href="/templates/admin/dist/css/adminlte.min.css">
</head>
<body class="hold-transition login-page">
<div class="login-box">
	<div class="login-logo">
		<a href="#"><b>Kenzo</b>Funny</a>
	</div>
	<!-- /.login-logo -->
	<div class="card">
		<div class="card-body login-card-body">
			<?php if (isset($message)): ?>
				<p class="login-box-msg" style="color: red;font-size: 14px;"><?= $message ?></p>
			<?php else: ?>
				<p class="login-box-msg">Đăng nhập để bắt đầu phiên của bạn</p>
			<?php endif; ?>
			<form action="<?= base_url('admin/login') ?>" method="post">
				<div class="input-group mb-3">
					<input type="text" name="username" class="form-control" placeholder="Tên tài khoản">
					<div class="input-group-append">
						<div class="input-group-text">
							<span class="fas fa-envelope"></span>
						</div>
					</div>
				</div>
				<div class="input-group mb-3">
					<input type="password" name="password" class="form-control" placeholder="Mật khẩu">
					<div class="input-group-append">
						<div class="input-group-text">
							<span class="fas fa-lock"></span>
						</div>
					</div>
				</div>
				<div class="row">
					<!--<div class="col-6">
					  <div class="icheck-primary">
						<input type="checkbox" id="remember">
						<label for="remember">
						  Ghi nhớ
						</label>
					  </div>
					</div>-->
					<!-- /.col -->
					<div class="col-6" style="margin-right: auto;margin-left: auto;">
						<button type="submit" class="btn btn-primary btn-block">Đăng nhập</button>
					</div>
					<!-- /.col -->
				</div>
			</form>
			<!-- /.social-auth-links -->

			<!--  <p class="mb-1">
				<a href="#">Quên mật khẩu</a>
			  </p>-->
		</div>
		<!-- /.login-card-body -->
	</div>
</div>
<!-- /.login-box -->

<!-- jQuery -->
<script src="/templates/admin/plugins/jquery/jquery.min.js"></script>
<!-- Bootstrap 4 -->
<script src="/templates/admin/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- AdminLTE App -->
<script src="/templates/admin/dist/js/adminlte.min.js"></script>

</body>
</html>
