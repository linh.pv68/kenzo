<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>Kenzo Funny | Dashboard</title>
	<!-- Tell the browser to be responsive to screen width -->
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<!-- Font Awesome -->
	<link rel="stylesheet" href="/templates/admin/plugins/fontawesome-free/css/all.min.css">
	<!-- Ionicons -->
	<link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
	<!-- Tempusdominus Bbootstrap 4 -->
	<link rel="stylesheet"
		  href="/templates/admin/plugins/tempusdominus-bootstrap-4/css/tempusdominus-bootstrap-4.min.css">
	<!-- iCheck -->
	<link rel="stylesheet" href="/templates/admin/plugins/icheck-bootstrap/icheck-bootstrap.min.css">
	<!-- Theme style -->
	<link rel="stylesheet" href="/templates/admin/dist/css/adminlte.min.css">
	<!-- overlayScrollbars -->
	<link rel="stylesheet" href="/templates/admin/plugins/overlayScrollbars/css/OverlayScrollbars.min.css">
	<!-- Google Font: Source Sans Pro -->
	<link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
</head>
<body class="hold-transition sidebar-mini layout-fixed">
<div class="wrapper">

	<!-- Navbar -->
	<?php $this->load->view("backend/navbar"); ?>
	<!-- /.navbar -->

	<!-- Main Sidebar Container -->
	<aside class="main-sidebar sidebar-dark-primary elevation-4">
		<?php $this->load->view("backend/sider_bar"); ?>
		<!-- /.sidebar -->
	</aside>

	<!-- Content Wrapper. Contains page content -->
	<div class="content-wrapper">
		<!-- Content Header (Page header) -->
		<?php $this->load->view("backend/header"); ?>
		<!-- /.content-header -->

		<!-- Modal -->
		<!-- Button trigger modal -->
		<button type="button" class="btn btn-primary" data-toggle="modal" id="popup" data-target="#exampleModal" hidden></button>

		<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
			<div class="modal-dialog" role="document">
				<div class="modal-content">
					<div class="modal-header">
						<h5 class="modal-title" id="exampleModalLabel">Thành Công</h5>
					</div>
					<div class="modal-body">
						Bạn đã sửa câu hỏi thành công
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-secondary" data-dismiss="modal">Đóng</button>
						<a type="button" class="btn btn-primary" href="<?=base_url().'question'?>">Tiếp Tục</a>
					</div>
				</div>
			</div>
		</div>

		<!-- Main content -->
		<section class="content">
			<div class="container-fluid">
				<div id="content">
					<h2>Sửa câu hỏi</h2>
					<div class="line"></div>
					<form id="form_edit_q" action="sua" style="max-width: 60%; margin: 0px auto;" enctype="multipart/form-data" method="post">
						<div class="form-group" hidden="hidden">
							<textarea type="text" class="form-control" placeholder="" id="id" name="id"><?= $question['id'] ?></textarea>
						</div>
						<div class="form-group">
							<label for="content	">Nội dung</label>
							<textarea type="text" class="form-control" placeholder="" id="content_q" name="content_q"><?= $question['content'] ?></textarea>
						</div>
						<div class="form-group">
							<label for="img	">Hình ảnh</label>
							<input type="file" class="form-control-file border" id="image_file" name="image_file" value="<?= $question['image'] ?>">
							<img src="<?= base_url('upload/images/').$question['image'] ?>" style="max-height: 100px; max-width: 100px;">
						</div>

						<div class="form-group">
							<label for="des">Mô tả</label>
							<textarea class="form-control" aria-label="With textarea" id="des" name="des"><?= $question['des'] ?></textarea>
						</div>
						<div class="form-group" style="position: relative;">
							<label for="ans">Đáp án</label>
							<div style="margin-bottom:10px;">
								<input type="text" class="form-control" placeholder="" id="answer1" style="display: inline-block; max-width: 80%;" name="answer1"  value="<?= $question['answer1'] ?>">
							</div>
							<div style="margin-bottom:10px;">
								<input type="text" class="form-control" placeholder="" id="answer2" style="display: inline-block; max-width: 80%;" name="answer2"  value="<?= $question['answer2'] ?>">
							</div>
							<div style="margin-bottom:10px;">
								<input type="text" class="form-control" placeholder="" id="answer3" style="display: inline-block; max-width: 80%;" name="answer3"  value="<?= $question['answer3'] ?>">
							</div>
							<div style="margin-bottom:10px;">
								<input type="text" class="form-control" placeholder="" id="answer4" style="display: inline-block; max-width: 80%;" name="answer4"  value="<?= $question['answer4'] ?>">
							</div>
							<div id="an_right" style="position: absolute; top: 45px; right: 0px;">
								<?php  
									if($question['correct']==1){
										echo '<input checked="checked" name="correct" type="radio" value="1" class="check" style="display: block;margin-bottom: 40px;"/>';
									}else{
										echo '<input  name="correct" type="radio" value="1" class="check" style="display: block;margin-bottom: 40px;"/>';
									}
									if($question['correct']==2){
										echo '<input checked="checked" name="correct" type="radio" value="2" class="check" style="display: block;margin-bottom: 35px;"/>';
									}else{
										echo '<input  name="correct" type="radio" value="2" class="check" style="display: block;margin-bottom: 35px;"/>';
									}
									if($question['correct']==3){
										echo '<input checked="checked" name="correct" type="radio" value="3" class="check" style="display: block;margin-bottom: 30px;"/>';
									}else{
										echo '<input  name="correct" type="radio" value="3" class="check" style="display: block;margin-bottom: 30px;"/>';
									}
									if($question['correct']==4){
										echo '<input checked="checked" name="correct" type="radio" value="4" class="check" style="display: block;"/>';
									}else{
										echo '<input  name="correct" type="radio" value="4" class="check" style="display: block;"/>';
									}

								?>
							</div>
						</div>
						<div class="form-group">
							<label for="code_id">Thuộc bộ câu hỏi</label>
							<select class="browser-default custom-select" id="code_id" name="code_id">
								<?php foreach ($option as $key => $value) 
									{ ?>
										<option value="<?= $value['id'] ?>" <?php if($value['id'] == $question['code_id']){echo 'selected';}?>><?= $value['title'] ?></option>
								<?php
									}
								?>
							</select>
						</div>
						<input type="submit" class="btn btn-primary" id="edit_q" value="Sửa"/>
					</form>
				</div>

			</div><!-- /.container-fluid -->
		</section>
		<!-- /.content -->
	</div>
	<!-- /.content-wrapper -->
	<?php $this->load->view("backend/footer"); ?>
	<!-- /.control-sidebar -->
</div>
<!-- ./wrapper -->

<!-- jQuery -->
<script src="/templates/admin/plugins/jquery/jquery.min.js"></script>
<!-- jQuery UI 1.11.4 -->
<script src="/templates/admin/plugins/jquery-ui/jquery-ui.min.js"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
	$.widget.bridge('uibutton', $.ui.button)
</script>
<!-- Bootstrap 4 -->
<script src="/templates/admin/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<script src="/templates/admin/dist/js/adminlte.js"></script>
<!-- <script type="text/javascript" src="<?php echo base_url("templates/js/question.js"); ?>"></script> -->
</body>
</html>


