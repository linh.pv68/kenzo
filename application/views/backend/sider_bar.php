<!-- Brand Logo -->
<a href="<?= base_url('admin/dashboard') ?>" class="brand-link">
	<img src="/templates/images/favicon.png" alt="AdminLTE Logo" class="brand-image img-circle elevation-3"
		 style="opacity: .8">
	<span class="brand-text font-weight-light">Kenzo Funny</span>
</a>

<!-- Sidebar -->
<div class="sidebar">
	<!-- Sidebar user panel (optional) -->
	<!--<div class="user-panel mt-3 pb-3 mb-3 d-flex">
		<div class="image">
			<img src="/templates/admin/dist/img/user2-160x160.jpg" class="img-circle elevation-2" alt="User Image">
		</div>
		<div class="info">
			<a href="#" class="d-block">Alexander Pierce</a>
		</div>
	</div>-->
	<!-- Sidebar Menu -->
	<nav class="mt-2">
		<ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
			<!-- Add icons to the links using the .nav-icon class
				 with font-awesome or any other icon font library -->
			<li class="nav-item has-treeview">
				<?php if (!isset($menu)): ?>
				<a href="<?= base_url() . 'admin/dashboard' ?>" class="nav-link active">
					<?php else: ?>
					<a href="<?= base_url() . 'admin/dashboard' ?>" class="nav-link">
						<?php endif; ?>
						<i class="nav-icon fas fa-tachometer-alt"></i>
						<p>
							Dashboard
						</p>
					</a>
			</li>
			<li class="nav-item has-treeview">
				<a href="<?= base_url() . 'code-question' ?>" class="nav-link">
					<i class="nav-icon  fas fa-briefcase"></i>
					<p>Quản lý bộ câu hỏi</p>
				</a>
			</li>

			<li class="nav-item has-treeview">
				<a class="nav-link">
					<i class="nav-icon fas fa-edit"></i>
					<p>
						Quản Lý Câu hỏi<i class="fas fa-angle-left right"></i>
					</p>
				</a>
				<ul class="nav nav-treeview">
					<li class="nav-item">
						<a href="<?= base_url() . 'question/add' ?>" class="nav-link">
							<i class="far fa-circle nav-icon"></i>
							<p>Thêm Mới</p>
						</a>
					</li>
					<li class="nav-item">
						<a href="<?= base_url() . 'question' ?>" class="nav-link">
							<i class="far fa-circle nav-icon"></i>
							<p>Danh Sách Câu Hỏi</p>
						</a>
					</li>
				</ul>
			</li>

			<li class="nav-item has-treeview">
				<a href="#" class="nav-link">
					<i class="nav-icon fas fa-user-circle"></i>
					<p>
						Người Dùng<i class="fas fa-angle-left right"></i>
					</p>
				</a>
				<ul class="nav nav-treeview">
					<li class="nav-item">
						<a href="<?= base_url('admin/list-user') ?>" class="nav-link">
							<i class="far fa-circle nav-icon"></i>
							<p>Danh Sách Người Dùng</p>
						</a>
					</li>
				</ul>
			</li>
			<li class="nav-item has-treeview ">
				<?php if (isset($menu) && $menu == 'charts'): ?>
				<a href="<?= base_url('admin/charts') ?>" class="nav-link active">
					<?php else: ?>
					<a href="<?= base_url('admin/charts') ?>" class="nav-link">
						<?php endif; ?>

						<i class="nav-icon  fas fa-chart-bar"></i>
						<p> Bảng Xếp Hạng</p>
					</a>
			</li>
		</ul>
	</nav>
	<!-- /.sidebar-menu -->
</div>
