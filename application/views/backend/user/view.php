<?php
if (!$this->session->userdata("userAdminInfo")) {
	return redirect(base_url() . 'admin/login');
}
?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>Kenzo Funny | Dashboard</title>
	<!-- Tell the browser to be responsive to screen width -->
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="icon" type="image/png" href="/templates/images/favicon.png"/>
	<!-- Font Awesome -->
	<link rel="stylesheet" href="/templates/admin/plugins/fontawesome-free/css/all.min.css">
	<!-- Ionicons -->
	<link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
	<!-- Tempusdominus Bbootstrap 4 -->
	<link rel="stylesheet"
		  href="/templates/admin/plugins/tempusdominus-bootstrap-4/css/tempusdominus-bootstrap-4.min.css">
	<!-- iCheck -->
	<link rel="stylesheet" href="/templates/admin/plugins/icheck-bootstrap/icheck-bootstrap.min.css">
	<!-- JQVMap -->
	<link rel="stylesheet" href="/templates/admin/plugins/jqvmap/jqvmap.min.css">
	<!-- Theme style -->
	<link rel="stylesheet" href="/templates/admin/dist/css/adminlte.min.css">
	<!-- overlayScrollbars -->
	<link rel="stylesheet" href="/templates/admin/plugins/overlayScrollbars/css/OverlayScrollbars.min.css">
	<!-- Daterange picker -->
	<link rel="stylesheet" href="/templates/admin/plugins/daterangepicker/daterangepicker.css">
	<!-- summernote -->
	<link rel="stylesheet" href="/templates/admin/plugins/summernote/summernote-bs4.css">
	<!-- Google Font: Source Sans Pro -->
	<link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
</head>
<body class="hold-transition sidebar-mini layout-fixed">
<div class="wrapper">

	<!-- Navbar -->
	<?php $this->load->view("backend/navbar"); ?>
	<!-- /.navbar -->

	<!-- Main Sidebar Container -->
	<aside class="main-sidebar sidebar-dark-primary elevation-4">
		<?php $this->load->view("backend/sider_bar"); ?>
		<!-- /.sidebar -->
	</aside>

	<!-- Content Wrapper. Contains page content -->
	<div class="content-wrapper">
		<!-- Content Header (Page header) -->
		<?php $this->load->view("backend/header"); ?>
		<!-- /.content-header -->

		<!-- Main content -->
		<section class="content">
			<div class="container-fluid">
				<button type="button" class="btn btn-primary" data-toggle="modal" id="popup" data-target="#exampleModal"
						hidden></button>

				<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog"
					 aria-labelledby="exampleModalLabel" aria-hidden="true">
					<div class="modal-dialog" role="document">
						<div class="modal-content">
							<div class="modal-header">
								<h5 class="modal-title" id="exampleModalLabel">Thành Công</h5>
							</div>
							<div class="modal-body">
								Bạn đã xóa thành công.
							</div>
							<div class="modal-footer">
								<button type="button" class="btn btn-primary" href="<?= base_url('admin/list-user') ?>"
										data-dismiss="modal">Đóng
								</button>
							</div>
						</div>
					</div>
				</div>
				<div id="content">
					<h2>Danh sách Tài Khoản</h2>
					<div class="container">
						<table class="table">
							<thead>
							<tr>
								<th>STT</th>
								<th>Email</th>
								<th>Họ Tên</th>
								<th>Mã Số Sinh Viên</th>
								<th>Tác vụ</th>
							</tr>
							</thead>
							<tbody>
							<?php if (isset($list_user) && $list_user): ?>
								<?php foreach ($list_user as $key => $value): ?>
									<tr>
										<td><?= $key + 1 ?></td>
										<td><?= $value['email'] ?></td>
										<td><?= $value['full_name'] ?></td>
										<td><?= $value['number_student'] ?></td>
										<td>
											<button type="button" class="btn btn-danger delete-user"
													id="delete_user_<?= $value['user_id'] ?>"
													onclick="deleteUser(<?= $value['user_id'] ?>)"
													style="padding: 4px 6px;">Xóa
											</button>
										</td>
									</tr>
								<?php endforeach; ?>

							<?php endif; ?>
							</tbody>
						</table>
					</div>

				</div>
			</div><!-- /.container-fluid -->
		</section>
		<!-- /.content -->
	</div>
	<!-- /.content-wrapper -->
	<?php $this->load->view("backend/footer"); ?>
	<!-- /.control-sidebar -->
</div>
<!-- ./wrapper -->

<!-- jQuery -->
<script src="/templates/admin/plugins/jquery/jquery.min.js"></script>
<!-- jQuery UI 1.11.4 -->
<script src="/templates/admin/plugins/jquery-ui/jquery-ui.min.js"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
	$.widget.bridge('uibutton', $.ui.button)
</script>
<!-- Bootstrap 4 -->
<script src="/templates/admin/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- Summernote -->
<script src="/templates/admin/plugins/summernote/summernote-bs4.min.js"></script>
<!-- overlayScrollbars -->
<script src="/templates/admin/plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js"></script>
<!-- AdminLTE App -->
<script src="/templates/admin/dist/js/adminlte.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="/templates/admin/dist/js/demo.js"></script>
<script>
	function deleteUser(userId) {
		var url = window.location.origin + '/admin/user/delete',
			data = {
				userId: userId
			},
			element = $("#delete_user_" + userId).parent('td').parent('tr');
		ajax(url, data, 'POST').done(function (response) {
			response = JSON.parse(response);
			if (response.success) {
				element.remove();
				jQuery(".modal-body").html("Bạn đã xóa thành công.");
				jQuery("#popup").click();
			}
			if (response.error) {
				jQuery(".modal-header").html("Thất Bại");
				jQuery(".modal-body").html(response.message);
				jQuery("#popup").click();
			}
		});

	}

	function ajax($url, $data, $type) {
		return $.ajax({
			url: $url,
			data: $data,
			type: $type,
		});
	}
</script>
</body>
</html>

