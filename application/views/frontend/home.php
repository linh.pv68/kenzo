<?php

?>
<!DOCTYPE html>
<html lang="en">
<head>
	<title>Trang Chủ</title>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<!--===============================================================================================-->
	<link rel="icon" type="image/png" href="/templates/images/favicon.png"/>
	<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="/templates/vendor/bootstrap/css/bootstrap.min.css">
	<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="/templates/fonts/font-awesome-4.7.0/css/font-awesome.min.css">
	<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="/templates/vendor/animate/animate.css">
	<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="/templates/vendor/css-hamburgers/hamburgers.min.css">
	<!--===============================================================================================-->
	<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="/templates/css/util.css">
	<link rel="stylesheet" type="text/css" href="/templates/css/main.css">
	<!--===============================================================================================-->
</head>
<body>
<div class="limiter">
	<div class="container-login100">
		<div class="wrap-login100">
			<div class="login100-pic js-tilt" data-tilt href="<?= base_url() ?>">
				<a href="<?= base_url() ?>">
					<img src="/templates/images/img-01.png" alt="IMG">
				</a>
			</div>
			<div class="login100-form validate-form">
					<span class="login100-form-title">
						Vui Lòng Nhập Mã Pin
					</span>

				<div class="wrap-input100 validate-input" data-validate="Vui lòng nhập mã pin để bắt đầu">
					<input class="input100" type="number" id="pincode" name="pincode" placeholder="Mã Pin">
					<span class="focus-input100"></span>
					<span class="symbol-input100">
							<i class="fa fa-lock" aria-hidden="true"></i>
						</span>
				</div>

				<div class="container-login100-form-btn">
					<button class="login100-form-btn" id="btn_start_game">
						Bắt Đầu Chơi
					</button>
				</div>

				<div class="text-center p-t-136">
					<a class="txt2" href="<?= base_url() . 'logout' ?>">
						Đăng xuất
						<i class="fa fa-long-arrow-left m-l-5" aria-hidden="true"></i>
					</a>
				</div>
			</div>
		</div>
		<div class="wrap-onboard" style="display: none">
			<div class="content-message-wait">
				<div>
					<p id="message-wait"> Vui Lòng Chờ Câu Hỏi!!!</p>
				</div>
			</div>
			<div class="content-game-start" style="display: none">
				<div class="time-set">
					<span class="time-life">30s</span>
					<span class="noti-time" style="display: none"></span>
				</div>
				<input id="questionId" value="" hidden/>
				<div class="content-game">
					<button type="button" id="anwsera" class="btn btn-primary btn-lg btn-block">Đáp án A</button>
					<button type="button" id="anwserb" class="btn btn-secondary btn-lg btn-block">Đáp án B</button>
					<button type="button" id="anwserc" class="btn btn-success btn-lg btn-block">Đáp án C</button>
					<button type="button" id="anwserd" class="btn btn-danger btn-lg btn-block">Đáp án D</button>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- Modal -->
<div class="modal fade" id="modalNotify" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="modalTitle">Title</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				...
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-primary" data-dismiss="modal">OK</button>
			</div>
		</div>
	</div>
</div>


<!--===============================================================================================-->
<script src="/templates/vendor/jquery/jquery-3.2.1.min.js"></script>
<!--===============================================================================================-->
<script src="/templates/vendor/bootstrap/js/popper.js"></script>
<script src="/templates/vendor/bootstrap/js/bootstrap.min.js"></script>
<script src="/templates/vendor/tilt/tilt.jquery.min.js"></script>
<script>
	$('.js-tilt').tilt({
		scale: 1.1
	})
</script>
<!--===============================================================================================-->
<script src="/templates/js/main.js"></script>

<script src="https://js.pusher.com/5.0/pusher.min.js"></script>
<script src="/templates/js/homepage.js"></script>
<script>
	<?php $user = $this->session->userdata("userInfo");
	$name = $user['full_name'];
	?>

	jQuery("#anwsera").on('click', function () {
		new sendAnswer(1);
	});
	jQuery("#anwserb").on("click", function () {
		new sendAnswer(2);
	});
	jQuery("#anwserc").on("click", function () {
		new sendAnswer(3);
	});
	jQuery("#anwserd").on("click", function () {
		new sendAnswer(4);
	});

	var sendAnswer = function (answer) {
		var base_url = window.location.origin;
		var button = ['anwsera', 'anwserb', 'anwserc', 'anwserd'];
		var codeQuestionInfo = JSON.parse(localStorage.getItem("codeQuestionInfo")),
			codequestion_id = codeQuestionInfo.id,
			question_id = $("#questionId").val();

		jQuery.ajax({
			url: base_url + '/answer-customer',
			type: 'POST',
			data: {
				user_id: <?=$user['user_id']?>,
				answer: answer,
				question_id: question_id,
				codequestion_id: codequestion_id
			}
		}).done(function (response) {
			// console.log(response);
			response = JSON.parse(response);
			if (response.success) {
				$(".modal-body").html(response.data.message);
				//disable Button;
				$.each(button, function (index, value) {
					$("#" + value).prop('disabled', true);
				});
			}
			if (response.error) {
				$(".modal-body").html(response.data.message);
			}
			$("#modalTitle").html("Thông báo");
			$('#modalNotify').modal('show');
		});

	};

</script>

</body>
</html>
