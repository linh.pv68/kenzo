<?php

?>
<!DOCTYPE html>
<html lang="en">
<head>
	<title> Đăng Ký</title>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<!--===============================================================================================-->
	<link rel="icon" type="image/png" href="/templates/images/favicon.png"/>
	<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="/templates/vendor/bootstrap/css/bootstrap.min.css">
	<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="/templates/fonts/font-awesome-4.7.0/css/font-awesome.min.css">
	<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="/templates/vendor/animate/animate.css">
	<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="/templates/vendor/css-hamburgers/hamburgers.min.css">
	<!--===============================================================================================-->
	<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="/templates/css/util.css">
	<link rel="stylesheet" type="text/css" href="/templates/css/main.css">
	<!--===============================================================================================-->
</head>
<body>

<div class="limiter">
	<div class="container-login100">
		<div class="wrap-login100">
			<div class="login100-pic js-tilt" data-tilt>
				<a href="<?=base_url()?>">
					<img src="/templates/images/img-01.png" alt="IMG">
				</a>
			</div>

			<form class="login100-form validate-form" action="register" method="post">
					<span class="login100-form-title">
						Tiến Hành Đăng ký
					</span>
				<?php if (isset($message)):?>
					<span class="system-message">
						<?=$message?>
					</span>
				<?php endif;?>

				<div class="wrap-input100 validate-input" data-validate = "Valid email is required: ex@abc.xyz">
					<input class="input100" type="text" name="email" placeholder="Địa chỉ Email">
					<span class="focus-input100"></span>
					<span class="symbol-input100">
							<i class="fa fa-envelope" aria-hidden="true"></i>
						</span>
				</div>
				<div class="wrap-input100 validate-input">
					<input class="input100" type="number" name="number_student" placeholder="Mã Sinh Viên">
					<span class="focus-input100"></span>
					<span class="symbol-input100">
							<i class="fa fa-graduation-cap" aria-hidden="true"></i>
						</span>
				</div>
				<div class="wrap-input100 validate-input">
					<input class="input100" type="text" name="full_name" placeholder="Họ Và Tên">
					<span class="focus-input100"></span>
					<span class="symbol-input100">
							<i class="fa fa-user" aria-hidden="true"></i>
						</span>
				</div>

				<div class="wrap-input100 validate-input" data-validate = "Password is required">
					<input class="input100" type="password" name="password" placeholder="Mật Khẩu">
					<span class="focus-input100"></span>
					<span class="symbol-input100">
							<i class="fa fa-lock" aria-hidden="true"></i>
						</span>
				</div>
				<div class="wrap-input100 validate-input" data-validate = "Password is required">
					<input class="input100" type="password" name="re_pass" placeholder="Nhập lại Mật Khẩu">
					<span class="focus-input100"></span>
					<span class="symbol-input100">
							<i class="fa fa-lock" aria-hidden="true"></i>
						</span>
				</div>

				<div class="container-login100-form-btn">
					<button class="login100-form-btn">
						Đăng ký
					</button>
				</div>
			</form>
		</div>
	</div>
</div>




<!--===============================================================================================-->
<script src="/templates/vendor/jquery/jquery-3.2.1.min.js"></script>
<!--===============================================================================================-->
<script src="/templates/vendor/bootstrap/js/popper.js"></script>
<script src="/templates/vendor/bootstrap/js/bootstrap.min.js"></script>
<!--===============================================================================================-->
<script src="/templates/vendor/tilt/tilt.jquery.min.js"></script>
<script >
	$('.js-tilt').tilt({
		scale: 1.1
	})
</script>
<!--===============================================================================================-->
<script src="/templates/js/main.js"></script>

</body>
</html>

