<?php


interface CustomModel
{
	public function add($data);
	public function delete($data);
	public function edit($data);
	public function getList();

}
