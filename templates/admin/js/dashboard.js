(function ($) {
	"use strict";
	// button start game
	var gameInfoFirst = JSON.parse(localStorage.getItem("gameInfo"));
	$("#start-game").on('click', function () {
		var pinCode = $("#pin_code").val(),
			gameInfo = {
				code_question_id: pinCode,
				code_title: $("#pin_code option:selected").text(),
				time: parseFloat($("#time_of_question").val())
			};

		localStorage.setItem("gameInfo", JSON.stringify(gameInfo));
		if (typeof pinCode == "undefined" || pinCode == "") {
			alert('Bạn cần chọn bộ câu hỏi để bắt đầu!');
			return false;
		}
	});
	if (localStorage.getItem("gameInfo")) {
		if (gameInfoFirst.code_title) {
			$(".code-title").html("Bộ câu Hỏi : " + gameInfoFirst.code_title);
			//$(".reset-scores").attr('href', urlResetScore);

		}
	}

	$(".reset-scores").click(function () {
		var urlResetScore = window.location.origin + '/admin/reset-score',
			data = {
				code_question_id :  gameInfoFirst.code_question_id
			};
		var resetScores =  confirm("Bạn muốn đặt lại điểm số cho bộ câu hỏi "+ gameInfoFirst.code_title);
		if (resetScores){
			ajax(urlResetScore, "POST", data).done(function (response) {
				response = JSON.parse(response);
				if (response.success){
					alert('Bạn đã xóa đặt lại điểm số công!');
				}
				if (response.error){
					alert(response.data.message);

				}
			})
		}
		//

	});

	function ajax($url, $type, $data) {
		return $.ajax({
			url: $url,
			type: $type,//POST Or GET
			data: $data,// using data have format object {}
		});
	}
})(jQuery);
