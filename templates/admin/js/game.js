(function ($) {
	"use strict";
	// button start game
	var base_url = window.location.origin,
		button = {
			start_game: $("#start-game")
		},
		question_id = $("#question_id").val(),
		answer_correct = parseFloat($("#answer_correct").val());
	var gameInfo = JSON.parse(localStorage.getItem("gameInfo")),
		time_game = 30;

	if (gameInfo.time) {
		$(".time-game").html(gameInfo.time + "s");
		time_game = gameInfo.time;
	}

	button.start_game.on('click', function () {
		startGame();
		$("#start-game").prop('disabled', true);
		var interval = setInterval(function () {
			var timeCount = $(".time-game").html();
			timeCount = parseFloat(timeCount);
			timeCount -= 1;
			$(".time-game").html(timeCount + "s");
			if (timeCount === 0) {
				clearInterval(interval);
				$(".time-game").html("Hết Giờ!");
				showAnswerCorrect();
			}
		}, 1000);
	});

	function getChartsScores() {
		var url = base_url + '/get-charts-scores',
			data = {
				question_id: question_id,
				codequestion_id: gameInfo.code_question_id
			};
		ajax(url, 'POST', data).done(function (response) {
			response = JSON.parse(response);
			console.log(response);
			if (response.success) {
				response.data;
				var htmlChart = '';
				$.each(response.data, function (key, value) {
					var number = key + 1;
					htmlChart +=
						"<li class=\"item\">" +
						"<div class=\"product-img\">" +
						"<span id='numberChart'>" + number +
						".</span>" +
						"</div>" +
						"<div class=\"product-info\">\n" +
						"<a href=\"javascript:void(0)\" class=\"product-title\">" + value['full_name'] + "<span class=\"badge badge-success float-right\">" + value['scores'] + "đ</span></a>\n" +
						"<span class=\"product-description\">" + value['number_student'] + "</span>\n" +
						"</div>" +
						"</li>";

				});
				$("#contentChartScores").html(htmlChart);
				$("#modalChartScores").modal("show");
			}
			if (response.error) {

			}
		})
	};

	function showAnswerCorrect() {
		switch (answer_correct) {
			case 1:
				blink("#anwsera");
				break;
			case 2:
				blink("#anwserb");
				break;
			case 3:
				blink("#anwserc");
				break;
			case 4:
				blink("#anwserd");
				break;

		}
		$("#modal-body").html()

	}

	function blink(element) {
		var time = 5;
		var interval = setInterval(function () {
			time -= 1;
			$(element).fadeTo(100, 0.1).fadeTo(200, 1.0);
			if (time === 0) {
				clearInterval(interval);
				getChartsScores();
			}
		}, 1000);
	}

	function startGame() {
		$.ajax({
			url: base_url + '/pusher',
			type: 'POST',
			data: {
				question_id: question_id,
				time_game: time_game
			},
		}).done(function (response) {
			console.log("Bắt đầu");
		});
	};

	function ajax($url, $type, $data) {
		return $.ajax({
			url: $url,
			type: $type,//POST Or GET
			data: $data,// using data have format object {}
		});
	}

})(jQuery);
