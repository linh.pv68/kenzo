$(document).ready(function () {

	// get líst
	$data = {};
	var baseUrl = window.location.origin;
	renderListCodeQuestion(baseUrl + '/code-question/get-list', $data);

	// Add code Question
	$('#addData').click(function () {
		execute('add');
	});
	$("#editData").click(function () {
		execute('edit');
	});


	function execute(method) {
		var urlAction = '';
		var code = $("#maBoCauHoi").val(),
			name = $("#tenBoCauHoi").val(),
			description = $("#moTaBoCauHoi").val();
		var data = {
			code: code,
			name: name,
			des: description
		};
		switch (method) {
			case 'add':
				urlAction = baseUrl + '/code-question/add';
				break;
			case 'edit':
				urlAction = baseUrl + '/code-question/edit';
				data.id = $('#getId').text();
				break;
		}

		if (code == "") {
			alert("Trường Mã không được để trống");
		} else if (name == "") {
			alert("Trường Tên không được để trống");
		} else {
			$("#list tr").remove();

			renderListCodeQuestion(urlAction, data);
			$("#maBoCauHoi").val("");
			$("#tenBoCauHoi").val("");
			$("#moTaBoCauHoi").val("");
			if (method == 'edit') {
				$('#addData').show();
				$('#editData').hide();
			}
		}
	};


	function renderListCodeQuestion($url, $data) {
		$.ajax({
			url: $url,
			data: $data,
			type: 'POST',
		}).done(function (response) {
			response = JSON.parse(response);
			if (response.success) {
				$("#list").html(response.data);
			}
			if (response.error) {
				alert(response.data.message);
			}

		});
	}
});


$(document).on('click', '.deleteData', function () {
	if (confirm("Bạn có chắc chắn muốn xóa?") == true) {
		var element = $(this).parent('td').parent('tr');
		var id = $(this).data('id');
		$url = 'del';
		$data = {id: id};
		$.ajax({
			url: window.location.origin + '/code-question/delete',
			data: $data,
			type: 'POST',
		}).done(function (response) {
			response = JSON.parse(response);
			if (response.success) {
				element.remove();
			}
			if (response.error) {
				alert(response.data.message);
			}

		});
	} else {
		return false;
	}
});

$(document).on('click', '.showDataEdit', function () {
	$('#addData').hide();
	$('#editData').show();
	var id = $(this).data('id');
	$('#getId').text(id);
	$url = 'edit';
	$data = {id: id};
	$.ajax({
		url: window.location.origin + '/code-question/get-by-id',
		data: $data,
		type: 'POST',
		datatype: 'json',
		success: function (data) {
			var show = $.parseJSON(data);
			$("#maBoCauHoi").val(show['code']);
			$("#tenBoCauHoi").val(show['title']);
			$("#moTaBoCauHoi").val(show['des']);
		}
	});
});
