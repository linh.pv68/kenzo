(function ($) {
	"use strict";
	var message = "Hết Giờ!";
	var base_url = window.location.origin;
	// Enable pusher logging - don't include this in production
	Pusher.logToConsole = false;
	var pusher = new Pusher('b9db9faad5a7b670f8d8', {
		cluster: 'ap1',
		forceTLS: true
	});
	var channel = pusher.subscribe('my-channel-kenzofunny-local');
	var button = {
		startGame: $("#btn_start_game")
	};

	channel.bind('my-event-kenzofunny-local', function (data) {
		// console.log(data);
		$("#questionId").val(data.question_id);
		$(".time-life").html(data.time_game + "s");
		managerButtonAnswer('enable');
		$(".content-message-wait").hide();
		$(".content-game-start").show();

		$(".noti-time").hide();
		//$(".time-life").html("30s");
		$(".time-life").show();
		var interval = setInterval(function () {
			var timeCount = $(".time-life").html();
			timeCount = parseFloat(timeCount);
			timeCount -= 1;
			if (timeCount === 0) {
				clearInterval(interval);
				$(".time-life").hide();
				$(".noti-time").show();
				$(".noti-time").html(message);
				managerButtonAnswer('disable');
			}
			$(".time-life").html(timeCount + "s");
		}, 1000);
	});


	button.startGame.on('click', function () {
		var pinCode = $("#pincode").val();

		checkPinCode(pinCode);
	});

	function checkPinCode(pinCode) {
		var urlCheckPinCode = base_url + '/code-question/check-pin-code',
			data = {
				pin_code: pinCode
			};
		ajax(urlCheckPinCode, 'POST', data).done(function (response) {
			if (response.success) {
				localStorage.setItem("codeQuestionInfo", JSON.stringify(response.data));
				$(".wrap-onboard").show();
				$(".wrap-login100").hide();
				managerButtonAnswer('disable');
				return response;
			}
			if (response.error) {
				$("#modalTitle").html("Thông báo");
				$(".modal-body").html(response.data.message);
				$('#modalNotify').modal('show');
				return false;
			}
		});
	}

	function ajax($url, $type, $data) {
		return $.ajax({
			url: $url,
			type: $type,//POST Or GET
			data: $data,// using data have format object {}
			dataType: 'Json'
		});
	}

	function managerButtonAnswer(action) {
		var button = ['anwsera', 'anwserb', 'anwserc', 'anwserd'];
		switch (action) {
			case 'disable':
				$.each(button, function (index, value) {
					$("#" + value).prop('disabled', true);
				});
				break;
			case 'enable':
				$.each(button, function (index, value) {
					$("#" + value).prop('disabled', false);
				});
				break;
		}
	}

})(jQuery);
