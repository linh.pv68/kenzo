$(document).ready(function () {
	var urlGetListOption = window.location.origin + '/get_list_option';

	/*$('.delete-question').on('click', function () {
		var questionId = $('#question_id').val(),
			url = window.location.origin + '/question/delete';
		var data = {
			id: questionId
		};
		var element = $(this).parent('td').parent('tr');

		console.log(questionId);
		ajax(url, data, 'POST').done(function (response) {
			response = JSON.parse(response);
			if (response.success) {
				element.remove();
				jQuery(".modal-body").val("Bạn đã xóa thành công.");
				jQuery("#popup").click();
			}
			if (response.error) {
				jQuery(".modal-header").val("Thất Bại");
				jQuery(".modal-body").val(response.message);
			}
		});

	});*/



	renderOption(urlGetListOption, {});


	$('#form_add_q').on('submit', function (e) {
		e.preventDefault();
		$.ajax({
			url: window.location.origin + '/question/add',
			method: "POST",
			data: new FormData(this),
			dataType: "json",
			contentType: false,
			cache: false,
			processData: false,
		}).done(function (response) {
			if (response.success) {
				jQuery("#popup").click();
			}
			if (response.error) {
				jQuery(".modal-header").val("Thất Bại");
				jQuery(".modal-body").val(response.message);
			}

		});
	});

	function ajax($url, $data, $type) {
		return $.ajax({
			url: $url,
			data: $data,
			type: $type,
		});
	}


	function renderOption($url, $data) {
		ajax($url, $data, 'POST').done(function (response) {
			response = JSON.parse(response);
			if (response.success) {
				$("#code_id").html(response.data);
			}
			if (response.error) {
				alert(response.data.message)
			}
		});
	}
});
